<?php

/**
 * @file
 * A custom handler for Country select via map.
 */

$plugin = array(
  'title' => t('Country select via world map'),
  'format callback' => 'addressfield_format_address_country_select',
  'type' => 'address',
  'weight' => -100,
);

function addressfield_format_address_country_select(&$format, $address, $context = array()) {
  if(arg(1) != 'ajax') {
    $custom_js = <<<EOT
 (function($)
   $("#world_map area").live('click', function(){
     _v = $(this).attr('code');
     $("select.country").val(_v).change();
     $("button#cboxClose").click();
   })
 )(jQuery)
EOT;
    
    drupal_add_js($custom_js, 'inline');
    drupal_add_library('jq_maphilight', 'jquery.maphilight');
    $_SESSION['world_map'] = 1;
  }
  $format['country']['#suffix'] = '<a href="?width=800&height=400&inline=true#world_map" class="colorbox-inline" >'.t('Select via map').'</a>';
}